#!/bin/sh

#+ Source [[https://gitlab.com/toms-dotfiles/utilities/-/blob/master/safe_linker.sh][safe_linker]]
safe_linker_url='https://gitlab.com/toms-dotfiles/utilities/-/raw/master/safe_linker.sh'
#-NOTE: POSIX compliant version of 'source <(curl -s "${safe_linker_url}")'
tmppipe=$(mktemp -u)
mkfifo -m 600 "${tmppipe}"
curl -s "${safe_linker_url}" >"${tmppipe}" &
. "${tmppipe}"

#+ Link the dots
safe_linker "$(pwd)"/.bash_profile "${HOME}"/.bash_profile
safe_linker "$(pwd)"/.bashrc "${HOME}"/.bashrc
safe_linker "$(pwd)"/.inputrc "${HOME}"/.inputrc
safe_linker "$(pwd)"/.shrc.d "${HOME}"/.shrc.d
safe_linker "$(pwd)"/.zimrc "${HOME}"/.zimrc
safe_linker "$(pwd)"/.zshrc "${HOME}"/.zshrc
