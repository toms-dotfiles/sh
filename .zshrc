#! /bin/bash

#+ Pathing
#- Source '"${HOME}"/.shrc.d/shrc.sh' if it exists
if [ -e "${HOME}"/.shrc.d/ ] ; then
  . "${HOME}"/.shrc.d/shrc.sh >/dev/null 2>&1
  . "${HOME}"/.shrc.d/path.sh >/dev/null 2>&1
  while IFS= read -r -d '' f ; do
    . "${f}" >/dev/null 2>&1
  done < <(find "${HOME}"/.shrc.d/* '!' -name 'shrc.sh' '!' -name 'path.sh' -print0)
fi

#- Check zsh version for compatipility with zim, fix issues if found
pkg='zsh'
current_ver="$("${pkg}" --version | awk '{print $2}')"
required_ver=5.2
if ! which "${pkg}" > /dev/null 2>&1 || [ "$(pkg_version "${current_ver}")" -lt "$(pkg_version "${required_ver}")" ] ; then
  cd /tmp || exit
  #- Check ncurses
  # TODO: Figure out how to path ncurses
  dep='ncurses'
  if ! which "${dep}" > /dev/null 2>&1 || [ ! -e "${HOME}"/local/include/"${dep}"/"${dep}".h ] ; then
    repo="ftp://ftp.gnu.org/gnu/${dep}/"
    releases="$(curl "${repo}" 2>&1 | awk -v dep=$dep '$0 ~ dep {print $9}')"
    latest="$(echo "${releases}" | grep -iv 'asc\|sig' | tail -1)"
    wget "${repo}"/"${latest}"
    tar xf "${dep}"*.tar.gz
    cd "${dep}"* || exit
    ./configure \
      --prefix="${HOME}"/local \
      CXXFLAGS="-fPIC" \
      CFLAGS="-fPIC"
    make -j
    make install
    #- Cleanup
    cd .. || exit
    rm -rf ./"${dep}"*
  fi
  #- Install latest zsh
  wget -O "${pkg}".tar.xz https://sourceforge.net/projects/zsh/files/latest/download
  tar xf "${pkg}".tar.xz
  cd "${pkg}"* || exit
  ./configure \
    --prefix="${HOME}"/local \
    CPPFLAGS="-I${HOME}/local/include" \
    LDFLAGS="-L${HOME}/local/lib"
  make -j
  make install
  #- Cleanup
  cd .. || exit
  rm -rf ./"${pkg}"*
  #- Path the new things
  # NOTE: pathing is automatic when using https://gitlab.com/toms-dotfiles/sh
fi

#+ Start Zsh IMproved FrameWork (Zim)[https://github.com/zimfw/zimfw], only if using zsh
if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" =~ zsh ]] ; then
  # Check if Zim is installed
  if [ ! -e "${HOME}"/.zim/ ] ; then
    printf "\e[0;33m:: Warning - Zsh IMproved FrameWork (Zim) is not installed.\e[0m\n"
    echo -n ":: Would you like to install Zim? [Y/n]: "
    read -r answer
    if [ "${answer}" != "${answer#[Yy]}" ] ; then
      mkdir -p "${HOME}"/.zim/
      wget -nv -O "${HOME}"/.zim/zimfw.zsh https://github.com/zimfw/zimfw/releases/latest/download/zimfw.zsh
      wget -nv -O "${HOME}"/.zshenv https://raw.githubusercontent.com/zimfw/install/9f0199d023c486f2e925ec8b05b000ecc282c9bf/src/templates/zshenv
      wget -nv -O "${HOME}"/.zlogin https://raw.githubusercontent.com/zimfw/install/9f0199d023c486f2e925ec8b05b000ecc282c9bf/src/templates/zlogin
      if [ ! -e "${HOME}"/.zshrc ] ; then
        wget -nv -O "${HOME}"/.zshrc https://raw.githubusercontent.com/zimfw/install/9f0199d023c486f2e925ec8b05b000ecc282c9bf/src/templates/zshrc
      fi
      if [ ! -e "${HOME}"/.zimrc ] ; then
        wget -nv -O "${HOME}"/.zimrc https://raw.githubusercontent.com/zimfw/install/9f0199d023c486f2e925ec8b05b000ecc282c9bf/src/templates/zimrc
        install_starship
      fi
      zsh "${HOME}"/.zim/zimfw.zsh install
      printf "\e[0;32m:: Success - opening a new zsh terminal to use Zim.\e[0m\n"
      exec zsh
    fi
  else
    # if starship (https://starship.rs/) is not installed, ask to install. else, use
    pkg=starship
    if which "${pkg}" > /dev/null 2>&1 ; then
      if [ ! -e /usr/local/bin/starship ] ; then
        install_starship
        printf "\e[0;32m:: Success - opening a new zsh terminal to use starship.\e[0m\n"
        exec zsh
      else
        #- init
        eval "$(starship init zsh)"
      fi
    fi
    # TODO: symlink a dotfile
    #+ Start configuration added by Zim {{{
    #
    # User configuration sourced by interactive shells
    #

    # -----------------
    # Zsh configuration
    # -----------------

    #
    # History
    #

    # Remove older command from the history if a duplicate is to be added.
    setopt HIST_IGNORE_ALL_DUPS

    #
    # Input/output
    #

    # Set editor default keymap to emacs (`-e`) or vi (`-v`)
    bindkey -e

    # Prompt for spelling correction of commands.
    #setopt CORRECT

    # Customize spelling correction prompt.
    #SPROMPT='zsh: correct %F{red}%R%f to %F{green}%r%f [nyae]? '

    # Remove path separator from WORDCHARS.
    WORDCHARS=${WORDCHARS//[\/]}


    # --------------------
    # Module configuration
    # --------------------

    #
    # completion
    #

    # Set a custom path for the completion dump file.
    # If none is provided, the default ${ZDOTDIR:-${HOME}}/.zcompdump is used.
    #zstyle ':zim:completion' dumpfile "${ZDOTDIR:-${HOME}}/.zcompdump-${ZSH_VERSION}"

    #
    # git
    #

    # Set a custom prefix for the generated aliases. The default prefix is 'G'.
    #zstyle ':zim:git' aliases-prefix 'g'

    #
    # input
    #

    # Append `../` to your input for each `.` you type after an initial `..`
    #zstyle ':zim:input' double-dot-expand yes

    #
    # termtitle
    #

    # Set a custom terminal title format using prompt expansion escape sequences.
    # See http://zsh.sourceforge.net/Doc/Release/Prompt-Expansion.html#Simple-Prompt-Escapes
    # If none is provided, the default '%n@%m: %~' is used.
    #zstyle ':zim:termtitle' format '%1~'

    #
    # zsh-autosuggestions
    #

    # Customize the style that the suggestions are shown with.
    # See https://github.com/zsh-users/zsh-autosuggestions/blob/master/README.md#suggestion-highlight-style
    #ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=10'

    #
    # zsh-syntax-highlighting
    #

    # Set what highlighters will be used.
    # See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters.md
    ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets)

    # Customize the main highlighter styles.
    # See https://github.com/zsh-users/zsh-syntax-highlighting/blob/master/docs/highlighters/main.md#how-to-tweak-it
    #typeset -A ZSH_HIGHLIGHT_STYLES
    #ZSH_HIGHLIGHT_STYLES[comment]='fg=10'

    # ------------------
    # Initialize modules
    # ------------------

    if [[ ${ZIM_HOME}/init.zsh -ot ${ZDOTDIR:-${HOME}}/.zimrc ]]; then
      # Update static initialization script if it's outdated, before sourcing it
      source ${ZIM_HOME}/zimfw.zsh init -q
    fi
    source ${ZIM_HOME}/init.zsh

    # ------------------------------
    # Post-init module configuration
    # ------------------------------

    #
    # zsh-history-substring-search
    #

    # Bind ^[[A/^[[B manually so up/down works both before and after zle-line-init
    bindkey '^[[A' history-substring-search-up
    bindkey '^[[B' history-substring-search-down

    # Bind up and down keys
    zmodload -F zsh/terminfo +p:terminfo
    if [[ -n ${terminfo[kcuu1]} && -n ${terminfo[kcud1]} ]]; then
      bindkey ${terminfo[kcuu1]} history-substring-search-up
      bindkey ${terminfo[kcud1]} history-substring-search-down
    fi

    bindkey '^P' history-substring-search-up
    bindkey '^N' history-substring-search-down
    bindkey -M vicmd 'k' history-substring-search-up
    bindkey -M vicmd 'j' history-substring-search-down
    # }}} End configuration added by Zim

    # export ZIM_HOME="${ZDOTDIR:-${HOME}}"/.zim
    # [[ -s "${ZIM_HOME}"/init.zsh ]] && . "${ZIM_HOME}"/init.zsh
  fi
fi

# TODO: sync bash, zsh, and maybe dash history


#+ Kubernetes
#- Check for kubectl
pkg=kubectl
if which "${pkg}" > /dev/null 2>&1 ; then
  #- Use kubectl autocomplete
  [[ $commands[kubectl] ]] && source <(kubectl completion zsh)
  alias k=kubectl
  #- Install Krew if not installed
  indep=krew
  if ! "${pkg}"-"${indep}" version > /dev/null 2>&1 ; then
    (
      set -x; cd "$(mktemp -d)" &&
      curl -fsSLO "https://github.com/kubernetes-sigs/krew/releases/latest/download/krew.{tar.gz,yaml}" &&
      tar zxvf krew.tar.gz &&
      KREW=./krew-"$(uname | tr '[:upper:]' '[:lower:]')_amd64" &&
      "$KREW" install --manifest=krew.yaml --archive=krew.tar.gz &&
      "$KREW" update
    )
    # export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
    # NOTE: pathing is automatic when using https://gitlab.com/toms-dotfiles/sh
    exec zsh
    # TODO: auto install krew packages if they are not installed
  fi
fi

#+ Terraform
pkg=terraform
if which "${pkg}" > /dev/null 2>&1 ; then
  autoload -U +X bashcompinit && bashcompinit
  complete -o nospace -C /usr/bin/terraform terraform
fi

#+ GCP
# The next line updates PATH for the Google Cloud SDK.
if [ -f "${HOME}/google-cloud-sdk/path.zsh.inc" ]; then source "${HOME}/google-cloud-sdk/path.zsh.inc"; fi
# The next line enables shell command completion for gcloud.
if [ -f "${HOME}/google-cloud-sdk/completion.zsh.inc" ]; then source "${HOME}/google-cloud-sdk/completion.zsh.inc"; fi
