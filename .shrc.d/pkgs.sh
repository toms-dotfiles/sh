#+ Kubernetes
#- Krew
# ctx # (kubectx)[https://github.com/ahmetb/kubectx]
# ns # (kubens)[https://github.com/ahmetb/kubectx]
# grep # [https://github.com/guessi/kubectl-grep]
# modify-secret # [https://github.com/rajatjindal/kubectl-modify-secret]
# sniff # [https://github.com/eldadru/ksniff]
# tree # [https://github.com/ahmetb/kubectl-tree]
# view-allocations # [https://github.com/davidB/kubectl-view-allocations]
