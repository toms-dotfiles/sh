#! /bin/bash

#+ Define functions
#- concatonate paths
pathconcat () {
  if ! echo "${PATH}" | grep -Eq "(^|:)${1}($|:)" ; then
    if [ "${2}" = "after" ] ; then
      export PATH="${PATH}":"${1}"
    else
      export PATH="${1}":"${PATH}"
    fi
  fi
}

#- Check version of package (POSIX)
pkg_version() {
  echo "$@" | awk -F. '{ printf("%d%03d%03d%03d\n", $1,$2,$3,$4); }'
}

#- Check for gnu packages on MacOS
gnu_path_check () {
    if [ -d /usr/local/opt/"${1}"/libexec/gnubin ] ; then
      pathconcat /usr/local/opt/"${1}"/libexec/gnubin
    fi
}

# WIP: Use gnu man pages for gnu packages on MacOS
man_swap () {
  find_alias="$(alias | grep "${1}.*=")"
  if [[ "${find_alias}" ]] ; then
    echo "${find_alias}"
  else
    # find_pkg="$(echo "${PATH}" | awk -F: -v pkg="[$1]" '{for(i=1;i<=NF;i++){if ($i ~ /pkg/){print $i}}}' | awk -F/ -v pkg="[$1]" '{for(i=1;i<=NF;i++){if ($i ~ /pkg/){print $i}}}')"
    find_pkg="$(echo "${PATH}" | grep -E "(${1}|IRF)[^:]*")"
    if [[ "${find_pkg}" ]] ; then
      echo "${find_pkg}"
    else
      echo 'false'
    fi
  fi
}

# Add support for 256 colors
export TERM="xterm-256color"

#+ inhibit clipman when using pass so passwords are not saved in xfce4-clipman-history
if [[ $(pgrep clipman) -ne 0 ]]; then
  function pass() {
    case $* in
      -c* )
        if [ -z $(xfconf-query --channel xfce4-panel --property=/plugins/clipman/tweaks/inhibit) ]; then
            echo "creating clipman inhibiter"
            xfconf-query --channel xfce4-panel --property=/plugins/clipman/tweaks/inhibit --create --type=bool --set=true
        else
            echo "inhibiting clipman"
            xfconf-query --channel xfce4-panel --property=/plugins/clipman/tweaks/inhibit --set=true
        fi
        if [[ $(xfconf-query --channel xfce4-panel --property=/plugins/clipman/tweaks/inhibit) != "true" ]];then
          echo "FAILED to set inhibit clipman, passwords will be saved to history"
        fi
        command pass "$@"
        echo "disabling clipman inhibiter"
        xfconf-query --channel xfce4-panel --property=/plugins/clipman/tweaks/inhibit --set=false ;;
      * )
        command pass "$@" ;;
    esac
  }
fi

#+ Fix common issues with WSL, if running in WSL
if grep -iq 'Microsoft' /proc/version ; then
  #- Set env vars
  HOME=~
  USER="$(whoami)"
  win_home=/c/Users/"$USER"
  #- use VcXsrv inplace of xserver
  export DISPLAY=localhost:0.0
  #- Pass (passwordstore.org)
  # Make `pass` (passwordstore.org) consistant when copying to clipboard
  function pass_gen_clip {
    # ensure an entry doesn't already exist, if so ask to overwrite
    if [[ $(pass find "$name" | wc -l) -eq 1 ]]; then
      # generate pass, strip all formatting, and  copy last line of output to clipboard the WSL way
      shift 2; command pass generate "$name" | awk 'NF <= 1' | sed 's/\x1b\[[0-9;]*m//g' | clip.exe
      printf "%s\n:: Generated and copied $name to clipboard.\n"
    else
      printf "%s\n:: Warning - An entry already exists for $name.\n"
      echo -n ":: Would you like to overwrite $name? [Y/n]: "
      read -r answer
      case $answer in
          [Yy]* )
            # overwrite, generate pass, strip all formatting, and  copy last line of output to clipboard the WSL way
            shift 2; command pass generate -f "$name" | awk 'NF <= 1' | sed 's/\x1b\[[0-9;]*m//g' | clip.exe
            printf "%s\n:: Generated and copied $name to clipboard.\n"
            ;;
          * )
          printf "%s\n:: $name unchanged.\n"
          ;;
      esac
    fi
  }
  function pass() {
    if [ -z "$*" ] ; then
      command pass ls .
    else
      name=$(echo "$@" | awk '{ print $NF }')
      case $* in
        -c* | --clip* )
          if [[ "$*" =~ generate ]] ; then
            pass_gen_clip "$@"
          else
          # copy first line of pass to clipboard the WSL way
          shift 1; command pass "$@" | awk 'FNR <= 1' | clip.exe
          printf "%s\n:: Copied first line of $name to clipboard.\n"
          echo ":: To see the rest of $name, use \`pass edit $name\`"
          fi
          ;;
        "show -c"* | "show --clip"* )
          # copy first line of pass to clipboard the WSL way
          shift 2; command pass "$@" | awk 'FNR <= 1' | clip.exe
          printf "%s\n:: Copied first line of $name to clipboard.\n"
          echo ":: To see the rest of $name, use \`pass edit $name\`"
          ;;
        generate* )
          if [[ "$*" =~ -c ]] ; then
            pass_gen_clip "$@"
          # TODO: handle other pass generate options
          # elif [[ "$*" =~ -f ]] || [[ "$*" =~ -i ]] || [[ "$*" =~ -n ]] ; then
          else
            command pass "$@"
          fi
          ;;
        * )
          # other options go unmodified
          command pass "$@"
          ;;
      esac
    fi
  }
  #- Python
  alias python='/usr/bin/python3'
fi

function install_starship() {
  echo -n ":: Would you like to install Starship Prompt? (https://starship.rs/) [Y/n]: "
  read -r answer
  if [ "${answer}" != "${answer#[Yy]}" ] ; then
    #- install
    if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" =~ zsh ]] ; then
      curl -fsSL https://starship.rs/install.sh | zsh
    elif [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" =~ bash ]] ; then
      curl -fsSL https://starship.rs/install.sh | bash
    fi
    #- write config
    # TODO: symlink a dotfile
    echo "# https://starship.rs/config/" >| ~/.config/starship.toml
    echo "[hostname]" >> ~/.config/starship.toml
    echo "ssh_only = true" >> ~/.config/starship.toml
    echo "disabled = false" >> ~/.config/starship.toml
    echo "" >> ~/.config/starship.toml
    echo "[directory]" >> ~/.config/starship.toml
    echo "truncation_symbol = '…/'" >> ~/.config/starship.toml
    echo "" >> ~/.config/starship.toml
    echo "[git_status]" >> ~/.config/starship.toml
    echo "style = 'bold purple'" >> ~/.config/starship.toml
    echo "" >> ~/.config/starship.toml
    echo "[kubernetes]" >> ~/.config/starship.toml
    echo "disabled = false" >> ~/.config/starship.toml
    echo "format = '[⎈](blue bold) [$context \($namespace\)](blue bold) in '" >> ~/.config/starship.toml
    #- init
    if [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" =~ zsh ]] ; then
      eval "$(starship init zsh)"
    elif [[ "$(ps -ef | awk '$2==pid' pid=$$ | awk -F/ '{print $NF}')" =~ bash ]] ; then
      eval "$(starship init bash)"
    fi
  fi
}

#+ GCP
export CLOUDSDK_PYTHON=$(which python3)