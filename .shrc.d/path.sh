#+ Default paths to set
pathconcat "${HOME}"/.bin after
pathconcat "${HOME}"/.local/bin after
pathconcat "${HOME}"/bin after
pathconcat /sbin after
pathconcat /usr/local/sbin
pathconcat /usr/local/bin
# pathconcat "${HOME}"/local/bin

#+ Environment Version Managers (eg: rbev)
#-NOTE: Works by finding all dirs with a `bin` subdir that match the regex below
envs=($(find "${HOME}" -maxdepth 1 -type d -iname "\.??env"))
for e in "${envs[@]}" ; do
  echo "${e}"
  if [ -d "${e}"/bin/ ] ; then
    pathconcat "${e}"/bin/ after
  fi
done

#+ Langs
#- golang
if [ -e /usr/bin/go ] || [ -e /usr/local/go/bin/ ]; then
  export GOPATH="${HOME}"/go
  export GOBIN="${GOPATH}"/bin
  if [ ! -e "${GOBIN}" ] ; then
    mkdir -p "${GOBIN}"
  fi
  if [ ! -e "${GOPATH}"/src ] ; then
    mkdir -p "${GOPATH}"/src
  fi
  if [ ! -e "${GOPATH}"/pkg ] ; then
    mkdir -p "${GOPATH}"/pkg
  fi
  pathconcat "${GOBIN}" after
  export GO111MODULE=on # needed to install gopls on arch linux
fi

#+ Misc Packages in ${HOME}
#- Krew
if [ -e "${HOME}"/.krew ] ; then
  pathconcat "${HOME}"/.krew/bin
fi

#+ Compiled Packages in ${HOME}/local/bin
if [ -e "${HOME}"/local/bin/ ] ; then
  pathconcat "${HOME}"/local/bin
fi

#+ OS specific
case "$(uname -s)" in
  "Linux" )
    true
    ;;
  "Darwin" )
    #- homebrew
    if [ -e /usr/local/bin/brew/ ] ; then
      pathconcat /usr/local/bin/brew
    fi
    # use gnu-sed with sed (must `brew install sed`)
    gnu_path_check gnu-sed
    # use gawk with awk cmd (must `brew install gawk`)
    gnu_path_check gawk
    # use gnu-grep with grep (must `brew install grep`)
    gnu_path_check grep
    # use gnu coreutils for things like gdate as date (must `brew install coreutils`)
    gnu_path_check coreutils
    ;;
esac

export PATH
